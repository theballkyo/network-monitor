from django.contrib import messages
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, logout, login
from .forms import AuthForm


def login_view(request):
    if request.method == 'POST':
        form = AuthForm(request.POST)

        if not form.is_valid():
            messages.warning(request, 'Username or password not match.')
            return render(request, 'authen/login.html', {'form': form})

        username = form.cleaned_data.get('username')
        password = form.cleaned_data.get('password')

        user = authenticate(request, username=username, password=password)
        if user is None:
            messages.warning(request, 'Username or password not match.')
        else:
            login(request, user)
            return redirect('list-deivce')
    else:
        form = AuthForm()

    return render(request, 'authen/login.html', {'form': form})


def logout_view(request):
    logout(request)


def register_view(request):
    pass